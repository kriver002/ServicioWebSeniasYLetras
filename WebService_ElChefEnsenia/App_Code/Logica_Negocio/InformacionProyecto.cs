﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descripción breve de InformacionProyecto
/// </summary>
public class InformacionProyecto
{
	public InformacionProyecto()
	{
		
	}
    float version = 0.0f;
    string nombreProyecto = "El chef enSeña";

    public string NombreProyecto
    {
        get { return nombreProyecto; }
        set { nombreProyecto = value; }
    }

    public float Version
    {
        get { return version; }
        set { version = value; }
    }
}