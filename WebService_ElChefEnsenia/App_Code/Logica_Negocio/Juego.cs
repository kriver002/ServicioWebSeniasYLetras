﻿
public class Juego
{
	private Personaje 	avatar = new Personaje();
	private bool		sonido = true;
	private bool		musica = true;
    private float versionLocal = 1.0f;

    public float VersionLocal
    {
        get { return versionLocal; }
        set { versionLocal = value; }
    }

	public Personaje Avatar
	{
		get { return avatar; }
		set { avatar = value; }
	}

	public bool Sonido
	{
		get { return sonido; }
		set { sonido = value; }
	}

	public bool Musica {
		get { return musica; }
		set { musica = value; }
	}
}
