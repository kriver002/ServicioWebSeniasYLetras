﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using ChefEnsenia.Managers;
using System.Web.Script.Services;
using System.Web.Script.Serialization;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// Para permitir que se llame a este servicio web desde un script, usando ASP.NET AJAX, quite la marca de comentario de la línea siguiente. 
// [System.Web.Script.Services.ScriptService]

public class Service : System.Web.Services.WebService
{
    public Service () {

        //Elimine la marca de comentario de la línea siguiente si utiliza los componentes diseñados 
        //InitializeComponent(); 
    }

    //[WebMethod]
    //public Palabra[] ObtenerPalabras()
    //{
    //    return PalabraManager.ObtenerPalabra().ToArray();
    //}

    [WebMethod]
    public Juego ObtenerInformacionElChefEnsenia()
    {
        Juego juego = new Juego();
        juego.Avatar.Palabras = PalabraManager.ObtenerPalabra();
        return juego;

    }

    [WebMethod]
    public float ActualizarVersion(float nuevaVersion, string password)
    {
        VersionManager.CrearJSON(nuevaVersion, password);
        //return GameInformation.LocalVersion;
        Juego juego = new Juego();
        return juego.VersionLocal;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    //[ScriptMethod(UseHttpGet = true, ResponseFormat = ResponseFormat.Json)]
    public string ObtenerVersion()
    {
        string[] a = new string[1];
        VersionManager.ObtenerVersion();
        //return GameInformation.LocalVersion;
        //a[0] = GameInformation.LocalVersion.ToString();

        Juego juego = new Juego();
        a[0] = juego.VersionLocal.ToString();
        //var jsonSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
        //string json = jsonSerializer.Serialize(a);
        //return json;
        return new JavaScriptSerializer().Serialize(a);
		
		
    }
}