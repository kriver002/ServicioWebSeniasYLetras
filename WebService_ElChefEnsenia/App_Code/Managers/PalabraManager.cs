﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ChefEnsenia.DataBase;

namespace ChefEnsenia.Managers
{
    /// <summary>
    /// Descripción breve de PersonajeManager
    /// </summary>
    public static class PalabraManager
    {
        public static List<Palabra> ObtenerPalabra()
        {
            return PalabraBD.ObtenerPalabras();
        }
    }
}
