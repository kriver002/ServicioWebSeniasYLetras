﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ChefEnsenia.DataBase;

namespace ChefEnsenia.Managers
{
    /// <summary>
    /// Descripción breve de PersonajeManager
    /// </summary>
    public static class AnimacionManager
    {
        public static Animacion ObtenerAnimacion(int idPalabra)
        {
            return AnimacionBD.ObtenerAnimacion(idPalabra);
        }

        public static SerializableDictionary<string, Animacion> ObtenerAnimaciones()
        {
            return AnimacionBD.ObtenerAnimaciones();
        }

    }



}
