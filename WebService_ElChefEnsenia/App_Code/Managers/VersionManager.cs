﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ChefEnsenia.DataBase;
using System.Xml;
using System.Web.Script.Serialization;
using System.IO;



namespace ChefEnsenia.Managers
{
    /// <summary>
    /// Descripción breve de VersionManager
    /// C:\Program Files (x86)\IIS Express
    /// C:\Users\Administrador\Documents\My Web Sites\WS_ChefEnsenia
    /// </summary>
    public static class VersionManager
    {
        public static void CrearJSON(float nuevaVersion, string pwd)
        {
            Juego juego = new Juego();
            JavaScriptSerializer ser = new JavaScriptSerializer();
            //float version = GameInformation.LocalVersion;
            float version = juego.VersionLocal;
            string password = "elchef";
            if (pwd == password)
            {
                InformacionProyecto ip = new InformacionProyecto();
                ip.Version = nuevaVersion;
                string ver = ser.Serialize(ip);
                File.WriteAllText("InformacionProyecto.json", ver);
                //GameInformation.LocalVersion = nuevaVersion;
                juego.VersionLocal = nuevaVersion;
            }
        }

        public static void ObtenerVersion()
        {
            JavaScriptSerializer ser = new JavaScriptSerializer();
            //string path = @"C:\Users\Administrador\Documents\My Web Sites\WS_ChefEnsenia\InformacionProyecto.json";
            string ver = File.ReadAllText("InformacionProyecto.json");
            InformacionProyecto ip = ser.Deserialize<InformacionProyecto>(ver);

            //GameInformation.LocalVersion = ip.Version;
            Juego juego = new Juego();
            juego.VersionLocal = ip.Version;
        }
    }
}
