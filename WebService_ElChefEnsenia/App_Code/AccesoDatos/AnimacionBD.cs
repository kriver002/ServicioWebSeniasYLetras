﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml.Serialization;
using MySql.Data.MySqlClient;


namespace ChefEnsenia.DataBase 
{
    /// <summary>
    /// Descripción breve de AdministradorBD
    /// </summary>
    public static class AnimacionBD
    {
        public static Animacion ObtenerAnimacion(int idPalabra)
        {
            MySqlConnection conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["cadenaConexion"].ConnectionString);
            Animacion animacionActual = new Animacion();

            string sql = string.Format(@"SELECT pal_id, pal_nombre, a.anim_id AS anim_id, a.anim_nombre AS anim_nombre
                                            FROM tbl_palabra as p LEFT JOIN tbl_animacion as a
                                            ON p.anim_id = a.anim_id
                                            WHERE a.anim_id IS NOT NULL
                                            AND p.pal_id = {0}", idPalabra); 

            conn.Open();
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader reader = cmd.ExecuteReader();

            Animacion objAnimacion = null;
            while (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("anim_id")))
                {
                    // Si no tiene animacion entonces objAnimacion debe ser NULL
                    objAnimacion = new Animacion();

                    objAnimacion.Id = reader.GetInt32(reader.GetOrdinal("anim_id"));
                    objAnimacion.Nombre = reader.GetString(reader.GetOrdinal("anim_nombre"));
                }
            }
            conn.Close();
            return objAnimacion;
        }

        public static SerializableDictionary<string, Animacion> ObtenerAnimaciones()
        {
            MySqlConnection conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["cadenaConexion"].ConnectionString);
            SerializableDictionary<string, Animacion> animaciones = new SerializableDictionary<string, Animacion>();

            string sql = string.Format(@"SELECT p.pal_nombre AS pal_nombre, a.anim_id AS anim_id, a.anim_nombre AS anim_nombre
                                         FROM tbl_animacion AS a, tbl_palabra AS p
                                         WHERE a.anim_id = p.anim_id");

            conn.Open();
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("anim_id")))
                {
                    // Si no tiene animacion entonces objAnimacion debe ser NULL
                    Animacion objAnimacion = new Animacion();

                    objAnimacion.Id = reader.GetInt32(reader.GetOrdinal("anim_id"));
                    objAnimacion.Nombre = reader.GetString(reader.GetOrdinal("anim_nombre"));

                    animaciones[reader.GetString(reader.GetOrdinal("pal_nombre"))] = objAnimacion;
                }
            }
            conn.Close();
            return animaciones;
        }

    }
}

