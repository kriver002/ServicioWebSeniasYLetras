﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml.Serialization;
using MySql.Data.MySqlClient;


namespace ChefEnsenia.DataBase 
{
    /// <summary>
    /// Descripción breve de AdministradorBD
    /// </summary>
    public static class SinonimoBD
    {
        public static List<Palabra> ObtenerSinonimos(int idPalabra)
        {
            MySqlConnection conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["cadenaConexion"].ConnectionString);
            List<Palabra> sinonimoActual = new List<Palabra>();

            string sql = string.Format(@"SELECT ps.pal_id, s.sin_nombre
                                         FROM tbl_palabra_sinonimo AS ps LEFT JOIN tbl_sinonimo AS s
                                         ON ps.sin_id = s.sin_id  
                                         AND ps.pal_id = {0}", idPalabra); 
            conn.Open();
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                if (!reader.IsDBNull(reader.GetOrdinal("sin_nombre")))
                {
                    Palabra objPalabra = new Palabra(reader.GetString(1));
                    objPalabra.Animarpalabra = null;
                    sinonimoActual.Add( objPalabra );
                }
            }
            conn.Close();
            return sinonimoActual;
        }
    }
}

