﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Xml.Serialization;
using MySql.Data.MySqlClient;
using ChefEnsenia.Managers;

namespace ChefEnsenia.DataBase 
{
    /// <summary>
    /// Descripción breve de AdministradorBD
    /// </summary>
    public static class PalabraBD
    {
        public static List<Palabra> ObtenerPalabras()
        {
            MySqlConnection conn = new MySqlConnection(ConfigurationManager.ConnectionStrings["cadenaConexion"].ConnectionString);
            List<Palabra> palabras = new List<Palabra>();
            string sql = @"SELECT pal_id, pal_nombre
                          FROM tbl_palabra";
                          
            conn.Open();
            MySqlCommand cmd = new MySqlCommand(sql, conn);
            MySqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                //Palabra palabraActual = new Palabra(reader.GetString(1));
                Palabra palabraActual = new Palabra( reader.GetString(reader.GetOrdinal("pal_nombre")) );
                int idPalabra = reader.GetInt32( reader.GetOrdinal("pal_id") );

                palabraActual.Animarpalabra = AnimacionManager.ObtenerAnimacion(idPalabra);
                palabraActual.sinonimos = SinonimoManager.ObtenerSinonimos(idPalabra);
                palabras.Add(palabraActual);
                //GameInformation.SeniasYLetras.Avatar.Animaciones = XMLManager.Deserialize(palabras.Add(palabraActual),"XMLPalabra");
            }
            conn.Close();
            return palabras;
        }
    }
}

